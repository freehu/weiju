package com.huixi.microspur.sysadmin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huixi.microspur.sysadmin.entity.SysMenu;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xzl
 * @since 2020-01-16
 */
public interface SysMenuService extends IService<SysMenu> {

}
