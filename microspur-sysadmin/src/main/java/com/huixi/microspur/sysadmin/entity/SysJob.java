package com.huixi.microspur.sysadmin.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author xzl
 * @since 2020-01-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sys_job")
@ApiModel(value="SysJob对象", description="")
public class SysJob implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId("id")
    private String id;

    @ApiModelProperty(value = "描述任务")
    @TableField("job_name")
    private String jobName;

    @ApiModelProperty(value = "任务表达式")
    @TableField("cron")
    private String cron;

    @ApiModelProperty(value = "状态:0未启动false/1启动true")
    @TableField("status")
    private Boolean status;

    @ApiModelProperty(value = "任务执行方法")
    @TableField("clazz_path")
    private String clazzPath;

    @ApiModelProperty(value = "其他描述")
    @TableField("job_desc")
    private String jobDesc;

    @TableField("create_by")
    private String createBy;

    @TableField("create_date")
    private LocalDateTime createDate;

    @TableField("update_by")
    private String updateBy;

    @TableField("update_date")
    private LocalDateTime updateDate;


}
