package com.huixi.microspur.sysadmin.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huixi.microspur.sysadmin.entity.SysRole;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xzl
 * @since 2020-01-16
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {

}
