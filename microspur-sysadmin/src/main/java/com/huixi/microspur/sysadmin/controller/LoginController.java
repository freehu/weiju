package com.huixi.microspur.sysadmin.controller;

import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author zhuxiaomeng
 * @since  2017/12/4.
 * 登录、退出页面
 */
@Controller
@Slf4j
@Api(value = "登录业务",description="登录校验处理")
public class LoginController {

    @GetMapping(value = "/login")
    public String loginCheck() {
        return "/login";
    }

    @GetMapping("/main")
    public String main() {
        return "main/main";
    }
}
