package com.huixi.microspur.sysadmin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huixi.microspur.sysadmin.entity.SysDictItem;

/**
 * <p>
 * 字典子表 服务类
 * </p>
 *
 * @author xzl
 * @since 2020-01-16
 */
public interface SysDictItemService extends IService<SysDictItem> {

}
