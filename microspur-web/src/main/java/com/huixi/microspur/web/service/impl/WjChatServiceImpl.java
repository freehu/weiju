package com.huixi.microspur.web.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huixi.microspur.web.entity.chat.WjChat;
import com.huixi.microspur.web.mapper.WjChatMapper;
import com.huixi.microspur.web.service.WjChatService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 聊天室（聊天列表） 服务实现类
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
@Service
public class WjChatServiceImpl extends ServiceImpl<WjChatMapper, WjChat> implements WjChatService {

}
