package com.huixi.microspur.web.controller.appeal;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.huixi.microspur.commons.base.BaseController;
import com.huixi.microspur.commons.util.wrapper.WrapMapper;
import com.huixi.microspur.commons.util.wrapper.Wrapper;
import com.huixi.microspur.web.entity.appeal.WjAppealTag;
import com.huixi.microspur.web.service.WjAppealTagService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 诉求-对应标签 前端控制器
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
@RestController
@RequestMapping("/wjAppealTag")
@Api(tags = "诉求标签的接口")
public class WjAppealTagController extends BaseController {

    @Autowired
    private WjAppealTagService wjAppealTagService;


    /**
     *  根据id 查询其标签
     * @Author 叶秋
     * @Date 2020/4/13 22:56
     * @param wjAppealTag
     * @return com.huixi.microspur.commons.util.wrapper.Wrapper
     **/
    public Wrapper queryWjAppealTagById(@RequestBody WjAppealTag wjAppealTag){

        QueryWrapper<WjAppealTag> objectQueryWrapper = new QueryWrapper<>();
        objectQueryWrapper.eq("appeal_id", wjAppealTag.getAppealId());

        List<WjAppealTag> list = wjAppealTagService.list(objectQueryWrapper);

        return WrapMapper.ok(list);

    }


}

