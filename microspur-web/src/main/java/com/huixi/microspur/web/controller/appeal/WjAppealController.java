package com.huixi.microspur.web.controller.appeal;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.huixi.microspur.commons.base.BaseController;
import com.huixi.microspur.commons.util.wrapper.WrapMapper;
import com.huixi.microspur.commons.util.wrapper.Wrapper;
import com.huixi.microspur.web.entity.VO.ListPageAppealVO;
import com.huixi.microspur.web.entity.VO.QueryAppealVO;
import com.huixi.microspur.web.entity.appeal.*;
import com.huixi.microspur.web.entity.user.WjUser;
import com.huixi.microspur.web.service.*;
import io.swagger.annotations.*;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 诉求表 前端控制器
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
@RestController
@RequestMapping("/wjAppeal")
@Api(tags = "诉求模块的接口")
public class WjAppealController extends BaseController {

    @Autowired
    private WjAppealService wjAppealService;

    @Autowired
    private WjAppealMaterialService wjAppealMaterialService;

    @Autowired
    private WjAppealTagService wjAppealTagService;

    @Autowired
    private WjAppealEndorseService wjAppealEndorseService;

    @Autowired
    private WjUserService wjUserService;


    /**
     * 添加 诉求数据
     *
     * @param wjAppeal
     * @return com.huixi.microspur.commons.util.wrapper.Wrapper
     * @Author 叶秋
     * @Date 2020/4/13 22:18
     **/
    @PostMapping("/addAppeal")
    @ApiModelProperty(value = "添加诉求")
    public Wrapper addAppeal(@RequestBody WjAppeal wjAppeal) {


        boolean save = wjAppealService.save(wjAppeal);


        return WrapMapper.ok();



    }


    /**
     *  按条件分页查询 诉求
     * @Author 叶秋
     * @Date 2020/2/26 21:14
     * @param listPageAppealVO
     **/
    @PostMapping("/listPageAppeal")
    @ApiOperation(value = "按条件分页查询 诉求")
    public Wrapper listPageAppeal(@RequestBody ListPageAppealVO listPageAppealVO){

        // 多表拼凑 数据
        ArrayList<QueryAppealVO> queryAppealVOS = new ArrayList<>();
        QueryAppealVO queryAppealVO ;

        List<WjAppeal> wjAppeals = wjAppealService.listPageAppeal(listPageAppealVO);

        for (WjAppeal wjAppeal : wjAppeals) {
            queryAppealVO = new QueryAppealVO();

            // 查询相关的用户信息
            WjUser byId = wjUserService.getById(wjAppeal.getUserId());
            queryAppealVO.setWjUser(byId);
            // 查询相关素材
            List<WjAppealMaterial> wjAppealMaterials = wjAppealMaterialService.listByAppealId(wjAppeal.getAppealId());
            queryAppealVO.setAppealMaterial(wjAppealMaterials);
            // 查询相关标签
            List<WjAppealTag> wjAppealTags = wjAppealTagService.listByAppealTag(wjAppeal.getAppealId());
            queryAppealVO.setAppealTag(wjAppealTags);
            // 判断是否已经点赞
            if(StrUtil.isNotEmpty(listPageAppealVO.getUserId())){
                Boolean endorse = wjAppealEndorseService.isEndorse(wjAppeal.getAppealId(), listPageAppealVO.getUserId());
                queryAppealVO.setIsEndorse(endorse);
            }
            // 获取点赞的总数量
            int totleCount = wjAppealEndorseService.getTotleCount(wjAppeal.getAppealId());
            wjAppeal.setEndorseCount(totleCount);


            BeanUtil.copyProperties(wjAppeal, queryAppealVO, true);
            queryAppealVOS.add(queryAppealVO);
        }


        return WrapMapper.ok(queryAppealVOS);
    }


    @GetMapping("/getByIdAppeal/{appealId}")
    @ApiOperation(value = "根据id 查询诉求")
    public Wrapper getByIdAppeal(@PathVariable String appealId) {

        QueryAppealVO queryAppealVO = new QueryAppealVO();

        // 根据id查询具体诉求
        WjAppeal byIdAppeal = wjAppealService.getById(appealId);
        // 查询相关的用户信息
        WjUser byIdUser = wjUserService.getById(byIdAppeal.getUserId());
        queryAppealVO.setWjUser(byIdUser);
        // 查询相关素材
        List<WjAppealMaterial> wjAppealMaterials = wjAppealMaterialService.listByAppealId(byIdAppeal.getAppealId());
        queryAppealVO.setAppealMaterial(wjAppealMaterials);
        // 查询相关标签
        List<WjAppealTag> wjAppealTags = wjAppealTagService.listByAppealTag(appealId);
        queryAppealVO.setAppealTag(wjAppealTags);

        BeanUtil.copyProperties(byIdAppeal, queryAppealVO, true);

        return WrapMapper.ok(queryAppealVO);

    }




}

