package com.weiju;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.huixi.microspur.web.WebApplication;
import com.huixi.microspur.web.entity.appeal.WjAppeal;
import com.huixi.microspur.web.mapper.TestMapper;
import com.huixi.microspur.web.service.TestService;
import com.huixi.microspur.web.service.WjAppealService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * Unit test for simple App.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WebApplication.class)
public class AppTest {

    @Autowired
    StringRedisTemplate stringRedisTemplate;

    @Autowired
    TestService testService;

    @Autowired
    TestMapper testMapper;

    @Autowired
    WjAppealService wjAppealService;

    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue() {

        stringRedisTemplate.opsForValue().set("hello","world");

    }


    @Test
    public void test1(){

        QueryWrapper<com.huixi.microspur.web.entity.Test> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByDesc("id");

        IPage<com.huixi.microspur.web.entity.Test> testIPage = new Page<>(1, 10);//参数一是当前页，参数二是每页个数
        testIPage = testService.page(testIPage, queryWrapper);

        List<com.huixi.microspur.web.entity.Test> records = testIPage.getRecords();
        for (com.huixi.microspur.web.entity.Test record : records) {
            System.out.println(record);
        }


    }

    @Test
    public void test2(){


    }


}
